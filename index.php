<?php
  // TEST POUR SAVOIR SI ON A UN UTLISATEUR CONNECTE
  $user = null;
  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
  if (!empty($_SESSION['user'])) {
    $user = $_SESSION['user'];
  }
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
        <link rel="stylesheet" href="style.css">
        <title>Mon animalerie</title>
    </head>
    <body>
        <?php 
            // ON A BESOIN D LA CONNEXION A LA BDD
            require('connect.php');
        ?>
        <h1>Mon animalerie</h1>
        <?php 

           
            // SI UTILISATUR CONNECTE AFFICHAGE DU LOGIN ET LIEN SE DECONNECTEUR
            // ET LIN POUR AJOUTER DES ANIMAUX
        if(empty($_SESSION['user'])) { ?>
        <a href="user-login.php">Se connecter </a><br><br>
        <?php } else {
            echo 'utilisateur connecté :'.$_SESSION['user'].' '; ?>
            <a href="user-logout.php">Se déconnecter </a><br><br>
            <!-- CREATE-->
            <a href="animal-create.php">Ajouter un animal </a><br>
        <?php } ?>
        <?php $choix=''; ?>
        <br>
        <form action="#" method="get">
        Rechercher par nom : <input type="text" name="choix" value=""> (taper <b>tous</b> pour tout avoir)
        <input type="submit" value="rechercher">
        </form>
        
        <table>
            <thead>
                <th>Nom</th>
                <th>Espèce</th>
                <th>Poids</th>
                <th>Date naissance</th>
                <th>Photo</th>
                <th>Histoire</th>
                <?php 
                if(!empty($_SESSION['user'])) {
                    echo '<th></th><th></th>';
                } ?>
            </thead>

            <?php

                 // POUR LA RCHERCHE
                if (!empty($_GET['choix']) && $_GET['choix']!=='tous') {
                    $sqlQuery = "SELECT * FROM animaux WHERE nom= :nom";
                    $query=$bdd->prepare($sqlQuery);
                    $query->execute([
                        "nom" => $_GET['choix']  
                    ]);
                    $data = $query->fetchAll();
                }

                else {

                    //READ DE TOUT LA TABLE
                    $query = $bdd->prepare('SELECT * FROM animaux');
                    $query->execute();
                    $data = $query->fetchAll();
                }
                foreach($data as $result){
                    ?>
                    <tr>
                        <td><?php echo $result['nom'] ?></td>
                        <td><?php echo $result['espece'] ?></td>
                        <td><?php echo $result['poids'] ?></td>
                        <!-- POUR L'AFFICHAGE DE LA DATE LA FONCTION strftime EST OBSOLT MIS PAS TROUV AUTRE CHOSE-->
                        <td><?php echo strftime('%d-%m-%Y',strtotime($result['date_naissance'])) ?></td>
                        <td><?php echo'<img src="'.$result['photo'].'">';?></td>
                        <td><?php echo $result['histoire'] ?></td>
                        
                            <?php
                            // UPDATE
                            // SI UTILISATUR CONNECTE
                            if(!empty($_SESSION['user'])) { ?>
                            <td><a href= <?php echo("animal-update.php?animal=".$result['id']); ?> > <i class="fa-solid fa-pen-to-square"></i></a></td>
                            <?php } 
                            // DELETE
                            // SI UTILISATUR CONNECTE
                            if(!empty($_SESSION['user'])) { ?>
                            <td><a href="animal-delete.php?animal=<?php echo $result['id'] ?>"><i class="fa-solid fa-trash-can"></i></a></td>
                            <?php } ?>
                        </td>
                </tr>       
            <?php } ?>
        </table> 
    </body>
</html>    
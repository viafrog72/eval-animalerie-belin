
<?php
require('connect.php');

if(!empty($_POST['nom'])) {
    $sqlQuery = "UPDATE animaux SET 
        nom=:nom,
        espece=:espece,
        poids=:poids,
        photo=:photo,
        histoire=:histoire,
        date_naissance=:date_naissance
        WHERE id=:animal";
    $query=$bdd->prepare($sqlQuery);
    $query->execute([
        "nom" => $_POST['nom'],
        "espece" => $_POST['espece'],
        "poids" => floatval($_POST['poids']),
        "photo" => $_POST['photo'],
        "histoire"  => $_POST['histoire'],
        "date_naissance" => date($_POST['dateNaissance']),
        "animal" =>$_GET['animal']
    ]);
    header('location:index.php');
    exit;
}


if(!empty($_GET['animal'])) {
    $sqlQuery = "SELECT * FROM animaux WHERE id= :animal";
    $query=$bdd->prepare($sqlQuery);
    $query->execute([
        "animal" => $_GET['animal']  
    ]);
    $result = $query->fetch();
?>


<form action="#" method="post">
    <label for="nom">Nom :</label>
    <input type="text" name="nom" value ="<?php echo $result['nom'] ?>"><br>
    <label for="espece">Espèce :</label>
    <select name="espece" >
        <option value="chien" <?php if ($result['espece']==='chien') {echo 'selected';} ?>>chien</option>
        <option value="chat"  <?php if ($result['espece']==='chat') {echo 'selected';} ?>>chat</option>
        <option value="oiseau"  <?php if ($result['espece']==='oiseau') {echo 'selected';} ?>>oiseau</option>
    </select><br>
    <label for="poids">Poids (kg) :</label>
    <input type="number" name="poids"  in="0" max="50" step="0.001" value ="<?php echo $result['poids'] ?>"><br>
    <label for="dateNaissance">Date naissance :</label>
    <input type="date" name="dateNaissance" value ="<?php echo $result['date_naissance'] ?>"><br>
    <label for="photo">Photo :</label>
    <input type="text" name="photo" value ="<?php echo $result['photo'] ?>" size="100"><br>
    <label for="histoire">Histoire :</label>
    <textarea name="histoire" rows="4" cols="55"><?php echo $result['histoire'] ?></textarea><br>
    <input type="submit" value="enregister">
</form>

<?php 
}
else {
    header('location:index.php');
    exit;
}
?>
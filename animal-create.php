<?php
require('connect.php');
$error='';
if (!empty($_POST['nom'])) {
    $sqlQuery = "SELECT * FROM animaux WHERE nom= :nom";
    $query=$bdd->prepare($sqlQuery);
    $query->execute([
        "nom" => $_POST['nom']  
    ]);
    $result = $query->fetch();
    if ($result) {
        $error="l'animal nommé ' ".$_POST['nom']." existe déjà";
    }
    else {
        $sqlQuery = "INSERT INTO animaux (nom, espece, poids, photo, histoire, date_naissance) 
            VALUES (:nom, :espece, :poids, :photo, :histoire, :date_naissance)";
        $query=$bdd->prepare($sqlQuery);
        $query->execute([
                "nom" => $_POST['nom'],
                "espece" => $_POST['espece'],
                "poids" => floatval($_POST['poids']),
                "photo" => $_POST['photo'],
                "histoire"  => $_POST['histoire'],
                "date_naissance" => date($_POST['dateNaissance'])
        ]);
        echo ('insertion réussie de '.$_POST['nom']);
        header('location:index.php');
        exit;
    }
}
?>

<form action="#" method="post">
    <label for="nom">Nom :</label>
    <input type="text" name="nom"><br>
    <label for="espece">Espèce :</label>
    <select name="espece" >
        <option value="chien">chien</option>
        <option value="chat">chat</option>
        <option value="oiseau">oiseau</option>
    </select><br>
    <label for="poids">Poids (kg) :</label>
    <input type="number" name="poids" in="0" max="50" step="0.001"><br>
    <label for="dateNaissance">Date naissance :</label>
    <input type="date" name="dateNaissance" value="2022-06-12"><br>
    <label for="photo">Photo :</label>
    <input type="text" name="photo" size="100"><br>
    <label for="histoire">Histoire :</label>
    <textarea name="histoire" rows="4" cols="55"></textarea><br>
    <input type="submit" value="enregister">
</form>
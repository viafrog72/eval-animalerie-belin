-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le : mer. 23 août 2023 à 12:27
-- Version du serveur : 10.10.2-MariaDB
-- Version de PHP : 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS 'zoocolliders';
CREATE DATABASE IF NOT EXISTS 'zoocolliders';
USE 'zoocolliders';

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `zoocolliders`
--

-- --------------------------------------------------------

--
-- Structure de la table `animaux`
--

DROP TABLE IF EXISTS `animaux`;
CREATE TABLE IF NOT EXISTS `animaux` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `poids` float(10,3) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `histoire` varchar(255) DEFAULT NULL,
  `espece` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


--
-- Déchargement des données de la table `animaux`
--

INSERT INTO `animaux` (`id`, `nom`, `poids`, `date_naissance`, `photo`, `histoire`, `espece`) VALUES
(1, 'felix', 2.500, '2023-02-07', 'https://media.istockphoto.com/id/1389420226/fr/photo/chat-du-bengale-allong%C3%A9-sur-le-canap%C3%A9-et-souriant.jpg?s=612x612&w=0&k=20&c=9xYGUJVpJ3W0ympv-UHfmvMxC0Q3-D7u-HeNdQbhQWk=', 'je suis un gentil petit chat abandonné qui recherche ses nouveaux maîtres.', 'chat'),
(11, 'titi', 0.025, '2023-08-16', 'https://media.istockphoto.com/id/690711198/fr/photo/oiseau-amusant.jpg?s=612x612&w=0&k=20&c=C1ksemjHjDnkB3_VQrxMRxhAJLMXOq2JP2MfU2Npf6Q=', 'gentil canari et ami de grosminet...', 'oiseau'),
(15, 'Medor', 10.500, '2021-07-23', 'https://cdn.pixabay.com/photo/2017/09/25/13/12/puppy-2785074_1280.jpg', 'chien très affectueux', 'chien'),
(17, 'minou', 2.101, '2023-06-12', '', '', 'chat');

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'luc', '$2y$10$OVSppfSILxiEpbkMD60u7ejQakkzCNZXCQNvUFoLZg4/cCMl.O2cG'),
(2, 'lea', '$2y$10$EyOlL15PEwpT0n5tCkGPNugMfBACzo0Zn2KvLvyg2.1FJoXKKk6Xq'),
(3, 'tom', '$2y$10$5N4cC.0oweL6ebAGjpkNF.4UvcZ74ES8vzsljzsGag2EFumtUX9f2');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

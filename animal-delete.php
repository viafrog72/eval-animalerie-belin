<?php
    require('connect.php');

    if(!empty($_GET['animal'])) {
        $sqlQuery = "DELETE FROM animaux WHERE id= :animal";
        $query=$bdd->prepare($sqlQuery);
        $query->execute([
            "animal" => $_GET['animal']  
        ]);
    }
    header('location:index.php');
    exit;
?>
<?php
$error='';
session_start();
require('connect.php');

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $query = $bdd->prepare('SELECT password FROM users WHERE login = :login');
    $query->execute([
       'login' => $_POST['login']
    ]);
    $data = $query->fetch();
    if ($data) {
        if (password_verify($_POST['password'], $data['password'])){
            
            $_SESSION['user'] = $_POST['login'];
            
            header('Location: index.php');
            exit;
        }
    } else {
        echo 'utilisateur et/ou mot de pass incorrect';}
}

?>

<form action="#" method="post">
    Identifiant: <input type="text" name="login" placeholder="login"><br>
    Mot de passe: <input type="password" name="password" placeholder="mot de passe"><br>
    <input type="submit" value="Se connecter"><br>
    <?php echo '<p class="error">'.$error.'</p>'; ?>
</form>
